sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageBox"
],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
	function (Controller,JSONModel,MessageBox) {
        "use strict";

        var sResponsivePaddingClasses = "sapUiResponsivePadding--header sapUiResponsivePadding--content sapUiResponsivePadding--footer";

		return Controller.extend("ejercicio.controller.View1", {
			onInit: function () {
                const oJSONModel = new JSONModel();
                const oView = this.getView();
                oJSONModel.loadData("./model/chartData.json");
                oView.setModel(oJSONModel,"ProductCollection");
                
            },
            
            onPress: function (oEvent) {
                const oItem = oEvent.getSource();
                const oBindingContext = oItem.getBindingContextPath();
                const oSettingsModel = this.getView().getModel('ProductCollection');
                const data = oSettingsModel.getProperty(oBindingContext);

                MessageBox.information("Informacion", {
                    title: "Informacion Producto: " + data.producto,
				    id: "messageBoxId1",
                    details: data,
                    contentWidth: "100px",
                    styleClass: sResponsivePaddingClasses
                });
            }
		});
	});
